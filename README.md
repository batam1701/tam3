
|Title | Delete student from the student list|
| ------ | ------ |
|**Value statement:**      | As an administrator  | 
||I want to remove students from the student list when not needed so that I can manage them more easily    |
| **Acceptance Criteria:**       |   ***Acceptance Criteria 1:***   |
||I have a list of existing students and I have a list of students absent from school|
||when I enter the student code it will be removed from the student list|
||then I will update the database again|
||***Acceptance Criteria 2:***|
||Given Students have paid tuition for the semester|
||When I enter the student code will be deleted|
||Then an error message will be displayed|
|**Definition of Done:**|Acceptance Criteria Met
||Test Cases Passes
||Code Reviewed
||Functional Tests Passed
||Non-Functional Requirement Met
||Product Owner Accepts User Story
|**Owner:**|Nguyen Ba Tam|
|**Iteration:**|Unscheduled|
|Estimate:|5 points|








